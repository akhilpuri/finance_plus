package com.angelcreations.divijbhatia.webkioskauthentication;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class MainActivity extends AppCompatActivity {
TextView msg;
    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        msg=(TextView)findViewById(R.id.message);
        button=(Button)findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            new setText().execute();


            }
        });
    }
    class setText extends AsyncTask<Void,Void,Void>
    {
        String text;
        ProgressDialog pd;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd=new ProgressDialog(MainActivity.this);
            pd.setMessage("Authenticating..");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Log.d("setText","Entered");
                text=GetText();
                Log.d("setText","Complete");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(pd.isShowing())
                pd.dismiss();
            msg.setText(text);
            Log.d("text",text);
        }
    }

    public String GetText() throws UnsupportedEncodingException
    {

       String i="JIIT",u="S",e="",d="",p="",x="",t="Institute",m="Member Type",c="Enrollment No",dob="DOB",pin="Password/Pin",s="Submit";

        String data = URLEncoder.encode("InstCode", "UTF-8")
                + "=" + URLEncoder.encode(i, "UTF-8");

        data += "&" + URLEncoder.encode("UserType", "UTF-8") + "="
                + URLEncoder.encode(u, "UTF-8");

        data += "&" + URLEncoder.encode("MemberCode", "UTF-8") + "="
                + URLEncoder.encode(e, "UTF-8");

        data += "&" + URLEncoder.encode("DATE1", "UTF-8") + "="
            + URLEncoder.encode(d, "UTF-8");

        data += "&" + URLEncoder.encode("Password", "UTF-8") + "="
            + URLEncoder.encode(p, "UTF-8");

        data += "&" + URLEncoder.encode("x", "UTF-8") + "="
                + URLEncoder.encode(x, "UTF-8");

        data += "&" + URLEncoder.encode("txtInst", "UTF-8") + "="
                + URLEncoder.encode(t, "UTF-8");

        data += "&" + URLEncoder.encode("txtuType", "UTF-8") + "="
                + URLEncoder.encode(m, "UTF-8");

        data += "&" + URLEncoder.encode("txtCode", "UTF-8") + "="
                + URLEncoder.encode(c, "UTF-8");

        data += "&" + URLEncoder.encode("DOB", "UTF-8") + "="
                + URLEncoder.encode(dob, "UTF-8");

        data += "&" + URLEncoder.encode("txtPin", "UTF-8") + "="
                + URLEncoder.encode(pin, "UTF-8");

        data += "&" + URLEncoder.encode("BTNSubmit", "UTF-8") + "="
                + URLEncoder.encode(s, "UTF-8");


        String text = "";
        BufferedReader reader=null;
        Log.d("Data","Sending");
        // Send data
        try
        {

            // Defined URL where to send data
            URL url = new URL("https://webkiosk.jiit.ac.in/CommonFiles/UserActionn.jsp");

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty( "Content-type", "application/x-www-form-urlencoded");
            conn.setRequestProperty( "Accept", "*/*" );
            /*URLConnection conn = url.openConnection();*/
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write( data );
            wr.flush();



            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;


            while((line = reader.readLine()) != null)
            {
                // Append server response in string
                sb.append(line);
            }


            text = sb.toString();
            Log.d("setText","text updated");
        }
        catch(Exception ex)
        {

        }
        finally
        {
            try
            {

                reader.close();
            }

            catch(Exception ex) {}
        }
        Log.d("message",text);


        /*text.trim();*/
        return text;


    }
}
