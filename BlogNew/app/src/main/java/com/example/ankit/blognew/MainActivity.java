package com.example.ankit.blognew;

import android.content.Context;
import android.media.Image;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.ankit.blognew.models.blogmodel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import dalvik.system.BaseDexClassLoader;

public class MainActivity extends AppCompatActivity {
    public ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Declare the button
        Button Retrieve = (Button)findViewById(R.id.hit);


        //Declare the list view
        lv = (ListView)findViewById(R.id.lv);

        //Set what ha sto be done when we click on the button

        Retrieve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new JSONTask().execute("http://financeplus.esy.es/test.php");

            }
        });



    }

    //Now we declare the task that has the task of getting the data frrom our hostinger remote server
    //password
    //ankit1111

    class JSONTask extends AsyncTask<String, String, List<blogmodel>> {


        @Override
        protected List<blogmodel> doInBackground(String... param) {


            HttpURLConnection conn = null;


            BufferedReader reader = null;


            try {

                URL url = new URL(param[0]);
                conn = (HttpURLConnection) url.openConnection();

                conn.connect();


                InputStream stream = conn.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();

                String line = "";

                while ((line = reader.readLine()) != null) {

                    buffer.append(line);
                }

                String FinalBuffer = buffer.toString();



                try {
                    JSONObject ParentObject = new JSONObject(FinalBuffer);

                    JSONArray Arr = ParentObject.getJSONArray("blog");

                    List<blogmodel> BlogModelList = new ArrayList<>();



                    for (int i=0; i<Arr.length();i++)
                    {
                        blogmodel lm = new blogmodel();

                        JSONObject FinalObject = Arr.getJSONObject(i);

                        lm.setTopic(FinalObject.getString("topic"));

                        lm.setBlog_name(FinalObject.getString("blog_name"));

                        lm.setSub(FinalObject.getString("subject"));

                        lm.setLikes_count(FinalObject.getInt("likes"));

                        lm.setText(FinalObject.getString("main_text"));

                        lm.setImage(FinalObject.getString("image"));







                        BlogModelList.add(lm);


                    }


                    return BlogModelList;
                } catch (JSONException e) {
                    e.printStackTrace();
                }





            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }







            finally {
                if (conn != null) {

                    conn.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


            return null;
        }

        @Override
        protected void onPostExecute(List<blogmodel> result) {
            super.onPostExecute(result);

            // Basically we have to set the display paramaeters here

            Blog_Adapter adapter = new Blog_Adapter(getApplicationContext(),R.layout.row,result);
            lv.setAdapter(adapter);
        }
    }



    public class Blog_Adapter extends ArrayAdapter {

        private List<blogmodel> BlogModelList;
        private int resource;
        private LayoutInflater inflater;

        public Blog_Adapter(Context context, int resource, List<blogmodel> objects) {
            super(context, resource, objects);
            BlogModelList = objects;
            this.resource = resource;
            inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null)
            {
                convertView  = inflater.inflate(resource,null);
            }

            TextView ta;
            TextView tb;
            TextView tc;
            TextView td;
            TextView tl;
            ImageView iv;


            ta=(TextView) convertView.findViewById(R.id.topic);
            tb=(TextView) convertView.findViewById(R.id.blog_name);
            tl=(TextView) convertView.findViewById(R.id.like_count);
            tc=(TextView) convertView.findViewById(R.id.sub);
            td=(TextView) convertView.findViewById(R.id.blog_text);
            iv=(ImageView)convertView.findViewById(R.id.imageView3) ;





            ta.setText(BlogModelList.get(position).getTopic());
            tb.setText(BlogModelList.get(position).getBlog_name());
            tc.setText(BlogModelList.get(position).getSub());
            td.setText(BlogModelList.get(position).getText());
            tl.setText(BlogModelList.get(position).getLikes_count());




            return convertView;
        }
    }

}
