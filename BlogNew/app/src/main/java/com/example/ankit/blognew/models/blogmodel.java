package com.example.ankit.blognew.models;

/**
 * Created by ankit on 10/10/16.
 */




public class blogmodel {

    //Declare all the types of variables matching in the db model in our hostinger phpmyadmin

    private String topic;
    private String blog_name;
    private String sub;
    private String text;
    private String image;
    private int likes_count;

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getBlog_name() {
        return blog_name;
    }

    public void setBlog_name(String blog_name) {
        this.blog_name = blog_name;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getLikes_count() {
        return likes_count;
    }

    public void setLikes_count(int likes_count) {
        this.likes_count = likes_count;
    }
}
