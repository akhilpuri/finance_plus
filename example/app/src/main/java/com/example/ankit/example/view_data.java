package com.example.ankit.example;

        import android.app.Activity;
        import android.app.ProgressDialog;
        import android.os.AsyncTask;
        import android.os.Bundle;
        import android.util.Log;
        import android.widget.ListView;

        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        import java.util.ArrayList;
        import java.util.HashMap;

public class view_data extends Activity {

    ListView lv;
    ArrayList<HashMap<String, String>> data;
    JsonClass jc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_data);
        lv=(ListView)findViewById(R.id.lv);

        new getData().execute();

    }

    class getData extends AsyncTask<Void, Void, Void>
    {
        ProgressDialog pd;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
          /*  pd=new ProgressDialog(ViewData.this);
            pd.setTitle("Processing");
            pd.setMessage("Loading...");
            pd.setIndeterminate(false);
            pd.setCancelable(true);
            pd.show();*/
        }

        @Override
        protected Void doInBackground(Void... voids) {
            data=new ArrayList();
            jc=new JsonClass();

            String result=jc.getData("http://www.ebook-shala.com/android/select.jsp");

            Log.d("data is comming to java",result);
            //Toast.makeText(getApplicationContext(),result.toString(),Toast.LENGTH_LONG).show();
            try {
                JSONArray ja=new JSONArray(result);
                //Toast.makeText(getApplicationContext(),result,Toast.LENGTH_LONG).show();
                for(int i=0;i<ja.length();i++)
                {
                    HashMap<String, String> map=new HashMap<>();
                    JSONObject jo=ja.getJSONObject(i);
                    String id=jo.getString("id");
                    String name=jo.getString("name");
                    String pass=jo.getString("pass");

                    map.put("id",id);
                    map.put("name",name);
                    map.put("pass",pass);
                    data.add(map);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

          /*  if (pd.isShowing()){

                pd.dismiss();
            }*/

            Adapter a=new Adapter(view_data.this,data);
            lv.setAdapter(a);
        }
    }
}
