package com.example.ankit.example;

        import android.app.Activity;
        import android.content.Context;
        import android.content.Intent;
        import android.os.Bundle;
        import android.support.annotation.Nullable;
        import android.util.AttributeSet;
        import android.view.View;
        import android.widget.Button;

public class MainActivity extends Activity {
    Button insert, view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        insert=(Button)findViewById(R.id.insertData);
        view=(Button)findViewById(R.id.viewData);

        insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this,insert.class);
                startActivity(i);

            }
        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this,view_data.class);
                startActivity(i);
            }
        });
    }
}
