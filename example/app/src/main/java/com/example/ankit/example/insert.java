package com.example.ankit.example;

        import android.app.ProgressDialog;
        import android.os.AsyncTask;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.Toast;

public class insert extends AppCompatActivity {
    EditText email,pass,cpass;
    Button insert;
    String result;
    JsonClass js;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);
        email=(EditText)findViewById(R.id.email);
        pass=(EditText)findViewById(R.id.pass);
        cpass=(EditText)findViewById(R.id.cpass);
        insert=(Button)findViewById(R.id.insert);



        insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String Email,Pass,Cpass;
                Email=email.getText().toString();
                Pass=pass.getText().toString();
                Cpass=cpass.getText().toString();
                if(Email.length()==0 )
                {
                    email.setError("Invalid");

                }
                else if(Pass.length()==0)
                {
                    pass.setError("Invalid");
                }
                if(Cpass.length()==0)
                {
                    cpass.setError("Invalid");
                }
                else if(!Pass.equals(Cpass))
                {
                    cpass.setError("Password don't match");
                }
                else
                {
                    url="http://ebook-shala.com/android/insert.jsp?name="+Email+"&pass="+Pass;
                    GetData gd=new GetData();
                    gd.execute();
                }
            }
        });
    }


    class GetData extends AsyncTask<Void,Void,Void>
    {
        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd=new ProgressDialog(insert.this);
            pd.setMessage("Please Wait.....");
            pd.setTitle("Processing");
            pd.setIndeterminate(false);
            pd.setCancelable(true);
            pd.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            js=new JsonClass();
            result= js.getData(url);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(pd.isShowing())
            {
                pd.dismiss();
            }
            Toast.makeText(getApplicationContext(),result,Toast.LENGTH_LONG).show();
        }
    }
}
