package com.example.divij.database;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity implements View.OnClickListener {
EditText name,email;
    Button submit,select;
    dbOperations db;
    WebView web;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        name=(EditText)findViewById(R.id.name);
        email=(EditText)findViewById(R.id.email);
        submit=(Button)findViewById(R.id.submit);
        select=(Button)findViewById(R.id.select);
        web=(WebView)findViewById(R.id.web);
        submit.setOnClickListener(this);
        select.setOnClickListener(this);
        web.loadUrl("www.ebook-shala.com");
    }

    @Override
    public void onClick(View v) {
        if(v==submit) {
            db = new dbOperations(MainActivity.this);
            int check = db.insert(name.getText().toString(), email.getText().toString());
            if (check == 0) {
            /*Toast.makeText(getApplicationContext(),"Menu 1 is selected",Toast.LENGTH_LONG).show();*/
                Toast.makeText(getApplicationContext(), "Insert Unsuccessful", Toast.LENGTH_LONG).show();
            } else if (check == 1) {
            /*Toast.makeText(getApplicationContext(),"Menu 1 is selected",Toast.LENGTH_LONG).show();*/
                Toast.makeText(getApplicationContext(), "Insert Successful", Toast.LENGTH_LONG).show();
            }
        }
        else if(v==select)
        {
            Intent i=new Intent(MainActivity.this,selectData.class);

            startActivity(i);
        }
    }


}
