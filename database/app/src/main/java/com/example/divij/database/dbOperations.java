package com.example.divij.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Divij on 16-Jun-16.
 */
public class dbOperations extends SQLiteOpenHelper{
    public static String dbname="Demo";
    public static int dbversion=1;
    public static String tbname="info";
    public static String id="pid";
    public static String name="pname";
    public static String email="pemail";

    public dbOperations(Context context) {
        super(context,dbname,null,dbversion);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    String query="create table "+tbname+" ( "+id+" integer primary key autoincrement, "+name+" varchar(50), "+email+" varchar(50) )";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public int insert(String n, String e)
    {
        try{

        SQLiteDatabase db=this.getWritableDatabase();
        /*ContentValues cv=new ContentValues();
        cv.put(name,n);
        cv.put(email,e);*/

        String query="insert into "+tbname+" ("+name+","+email+") values('"+n+"','"+e+"');";
            /*String query="delete from "+tbname+" where "+id+" =3;";*/
            /*String query ="update "+tbname+" set "+name+"='Dharmit' where "+id+"=5;";*/
        db.execSQL(query);
        return 1;
        }
        catch (Exception ex)
        {
            return 0;
        }
    }

    public Cursor select(String[] n)
    {
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor c=db.rawQuery("select "+id+" as _id,* from "+tbname+" where "+id+">=? ;",n);
        c.moveToFirst();
        return c;

    }

}
