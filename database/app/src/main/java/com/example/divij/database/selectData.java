package com.example.divij.database;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

/**
 * Created by Divij on 17-Jun-16.
 */
public class selectData extends ActionBarActivity{

    ListView lv;
    dbOperations db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selectdata);
        lv=(ListView)findViewById(R.id.data);
        db=new dbOperations(selectData.this);
        String []s={"5"};
        Cursor c=db.select(s);

        startManagingCursor(c);
        String [] from={dbOperations.id,dbOperations.name,dbOperations.email};
        int [] to={R.id.id,R.id.name,R.id.email};

        final Adapt a=new Adapt(selectData.this,R.layout.singledata,c,from,to);
        lv.setAdapter(a);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(),a.getItem(position).toString(),Toast.LENGTH_LONG).show();
            }
        });

        registerForContextMenu(lv);

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0,0,0,"a");
        menu.add(0,1,2,"c");
        menu.add(0,2,1,"b");
        menu.add(0,3,3,"d");

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Toast.makeText(getApplicationContext(),item.toString()+" is Selected ",Toast.LENGTH_LONG).show();
        return false;
    }

    class Adapt extends SimpleCursorAdapter
    {
        public int layout;
        Context context;


        public Adapt(Context context, int layout, Cursor c, String[] from, int[] to) {
            super(context, layout, c, from, to);
            this.layout=layout;
            this.context=context;
        }
    }
}
