package com.angelcreations.divijbhatia.dynamiclistview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
EditText msg;
    ListView list;
    ArrayList<String> msgs;
    Button send;
    ArrayAdapter msgAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        msg=(EditText)findViewById(R.id.msg);
        send=(Button)findViewById(R.id.send);
        list=(ListView)findViewById(R.id.list);
        msgs=new ArrayList<String>();


        msgAdapter=new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_list_item_1,msgs);
        list.setAdapter(msgAdapter);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                msgs.add(msg.getText().toString());
                list.setAdapter(msgAdapter);
                msg.setText("");
            }
        });
    }
}
