package com.example.ankit.test_app;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.ankit.test_app.models.LoginModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {



    public ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        Button Retrieve = (Button) findViewById(R.id.hit);

        lv = (ListView)findViewById(R.id.login_list);



        Retrieve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new JSONTask().execute("http://financeplus.esy.es/test.php");

            }
        });


    }


    class JSONTask extends AsyncTask<String, String, List<LoginModel>> {


        @Override
        protected List<LoginModel> doInBackground(String... param) {


            HttpURLConnection conn = null;


            BufferedReader reader = null;


            try {

                URL url = new URL(param[0]);
                conn = (HttpURLConnection) url.openConnection();

                conn.connect();


                InputStream stream = conn.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();

                String line = "";

                while ((line = reader.readLine()) != null) {

                    buffer.append(line);
                }

                String FinalBuffer = buffer.toString();



                try {
                    JSONObject ParentObject = new JSONObject(FinalBuffer);

                    JSONArray Arr = ParentObject.getJSONArray("login_list");

                    List<LoginModel> LoginModelList = new ArrayList<>();



                    for (int i=0; i<Arr.length();i++)
                    {
                        LoginModel lm = new LoginModel();

                        JSONObject FinalObject = Arr.getJSONObject(i);

                        lm.setUsr(FinalObject.getString("username"));

                        lm.setPwd(FinalObject.getString("password"));



                            LoginModelList.add(lm);


                    }


                    return LoginModelList;
                } catch (JSONException e) {
                    e.printStackTrace();
                }





            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }







            finally {
                if (conn != null) {

                    conn.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


            return null;
        }

        @Override
        protected void onPostExecute(List<LoginModel> result) {
            super.onPostExecute(result);

           // Basically we have to set the display paramaeters here

            LoginAdapter adapter = new LoginAdapter(getApplicationContext(),R.layout.row,result);
            lv.setAdapter(adapter);
        }
    }

    public class LoginAdapter extends ArrayAdapter{

        private List<LoginModel> loginModelList;
        private int resource;
        private LayoutInflater inflater;

        public LoginAdapter(Context context, int resource, List<LoginModel> objects) {
            super(context, resource, objects);
            loginModelList = objects;
            this.resource = resource;
            inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null)
            {
                convertView  = inflater.inflate(resource,null);
            }

            TextView usr;
            TextView pwd;

            usr=(TextView) convertView.findViewById(R.id.tva);
            pwd=(TextView) convertView.findViewById(R.id.tvb);

            usr.setText(loginModelList.get(position).getUsr());
            pwd.setText(loginModelList.get(position).getPwd());

            return convertView;
        }
    }
}

