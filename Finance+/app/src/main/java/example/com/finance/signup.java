package example.com.finance;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class signup extends AppCompatActivity {
    EditText name,pass,confirm,emailid,userid,dob,phone;
    Button Sign;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        name = ( EditText) findViewById(R.id. name);
        pass = ( EditText) findViewById(R.id. password);
        confirm = ( EditText) findViewById(R.id. cpassword);
         emailid= ( EditText) findViewById(R.id. email);
        userid = ( EditText) findViewById(R.id. userid);
        dob= ( EditText) findViewById(R.id. dob);
        phone = ( EditText) findViewById(R.id. phone);

        Sign = (Button) findViewById(R.id.sign);
        Sign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String n = name.getText(). toString();
                String p = pass.getText(). toString();
                String c = confirm.getText(). toString();
                String e = emailid.getText(). toString();
                String u = userid.getText(). toString();
                String d = dob.getText(). toString();
                String ph = phone.getText(). toString();

                if (n.isEmpty() || n.length() < 2) {
                    name.setError("at least 2 characters");
                } else {
                    name.setError(null);
                }
                if (p.isEmpty() || p.length() < 4 || p.length() > 10) {
                    pass.setError("between 4 and 10 alphanumeric characters");
                } else {
                    pass.setError(null);
                }
                if (c.isEmpty() || c.length() < 4 || c.length() > 10)
                    confirm.setError("between 4 and 10 alphanumeric characters");
                else if (c.compareToIgnoreCase(p)!=0){
                    confirm.setError("Password does not match");
                }
                else
                {
                    confirm.setError(null);
                }
                if (u.isEmpty() || u.length() < 3) {
                    userid.setError("at least 3 characters");
                } else {
                    userid.setError(null);
                }
                if (e.isEmpty() || e.length() < 7 ) {
                    emailid.setError("at least 7 characters");
                } else {
                    emailid.setError(null);
                }
                if (d.isEmpty() || d.length() !=8) {
                    dob.setError("Enter 8 characters(ddmmyyyy)");
                } else {
                    dob.setError(null);
                }   if (ph.isEmpty() || ph.length() != 10) {
                    phone.setError("Enter 10 digit number");
                } else {
                    phone.setError(null);
                }

            }
        });
    }

}