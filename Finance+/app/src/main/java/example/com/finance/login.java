package example.com.finance;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class login extends AppCompatActivity {
    EditText u,pass;
    Button Login;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        u= ( EditText) findViewById(R.id. userid);
        pass = ( EditText) findViewById(R.id. password);
        Login = (Button) findViewById(R.id.login);

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user = u.getText(). toString();
                String passw = pass.getText(). toString();


                if (user.isEmpty() || user.length() < 3) {
            u.setError("at least 3 characters");
        } else {
            u.setError(null);
        }
        if (passw.isEmpty() || passw.length() < 4 || passw.length() > 10) {
            pass.setError("between 4 and 10 alphanumeric characters");
        } else {
            pass.setError(null);
        }
    }
});
        }
        }


