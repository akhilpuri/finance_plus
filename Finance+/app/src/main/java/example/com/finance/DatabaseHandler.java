package example.com.finance;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import static android.R.attr.name;
import static example.com.finance.R.id.cpassword;
import static example.com.finance.R.id.dob;
import static example.com.finance.R.id.email;
import static example.com.finance.R.id.password;
import static example.com.finance.R.id.phone;
import static example.com.finance.R.id.userid;

public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "user";
    private static final String TABLE_NAME = "new";
    private static final String KEY_ID = "userid";
    private static final String KEY_NAME = "name";
    private static final String KEY_PASSWORD = "password";
    private static final String KEY_CPASSWORD = "cpassword";
    private static final String KEY_EMAILID = "email";
    private static final String KEY_DOB = "dob";
    private static final String KEY_PH_NO = "phone";

    public DatabaseHandler(Activity context) {
        super(context, DB_NAME, null, DB_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql= "create table "+TABLE_NAME+"("+KEY_ID+" text primary key ,"+KEY_NAME+" text,"+KEY_PASSWORD+" password,"+KEY_CPASSWORD+" password,"+KEY_EMAILID+" text,"+KEY_DOB+" date,"+KEY_PH_NO+" integer)";
        db.execSQL(sql);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql=("drop table if exist "+TABLE_NAME);
        db.execSQL(sql);
        onCreate(db);
    }
    public void insert(signup signup){
        SQLiteDatabase db= this.getWritableDatabase();
        ContentValues Values= new ContentValues();
        Values.put(KEY_NAME, name);
        Values.put(KEY_ID, userid);
        Values.put(KEY_CPASSWORD, cpassword);
        Values.put(KEY_EMAILID, email);
        Values.put(KEY_DOB, dob);
        Values.put(KEY_PASSWORD, password);
        Values.put(KEY_PH_NO, phone);
        db.insert("user", null, Values);
        db.close();

    }

}