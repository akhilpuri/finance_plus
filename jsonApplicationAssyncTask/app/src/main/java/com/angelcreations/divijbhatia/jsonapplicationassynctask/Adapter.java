package com.angelcreations.divijbhatia.jsonapplicationassynctask;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Divij Bhatia on 29-06-2016.
 */
public class Adapter extends BaseAdapter {

    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String,String>> data;

    public Adapter(Context context,ArrayList<HashMap<String,String>> data)
    {
        this.context=context;
        this.data=data;

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v=inflater.inflate(R.layout.single_data,viewGroup,false);

        TextView name, pass,id;
        name=(TextView)v.findViewById(R.id.name);
        pass=(TextView)v.findViewById(R.id.pass);
        id=(TextView)v.findViewById(R.id.id);

        HashMap<String, String > map=data.get(i);
        String id1=map.get("id");
        String n=map.get("name");
        String p=map.get("pass");
        id.setText(id1);
        name.setText(n);
        pass.setText(p);

        return v;
    }
}
